'use strict';

const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
// register global shortcuts
const globalShortcut = electron.globalShortcut;
// fs-extra and path
const fs = require('fs-extra');
const path = require('path');

// MAIN object
let main = {
  windows: {},
  settings: {
    width: 800, 
    height: 600,
    center: true,
    frame: false, 
    transparent: true,
    title: 'Invoicer',
    icon: "./app/img/icon_64.png"
  },
  init: function(){
    var self = this;
    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    app.on('ready', function(){
      self.createSplash();
      self.loadSettings(function(settings){
        self.createWindow(settings);
      });
    });
    // Quit when all windows are closed.
    app.on('window-all-closed', function () {
      // On OS X it is common for applications and their menu bar
      // to stay active until the user quits explicitly with Cmd + Q
      if (process.platform !== 'darwin') {
        app.quit();
      }
    });
    app.on('activate', function () {
      // On OS X it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (windows.main === null) {
        self.createWindow();
      }
    });
    app.on('will-quit', function() {
      // Unregister all shortcuts.
      //globalShortcut.unregisterAll();
    });
  },
  createSplash: function(){
    var self = this;
    var winConfig = {
      width: 500, 
      height: 500,
      center: true,
      frame: false,
      title: 'Invoicer',
      icon: "./app/img/icon_64.png",
      transparent: true,
      backgroudColor: '#00FFFFFF',
      resizable: false
    };
    //
    self.windows.splash = new BrowserWindow(winConfig);
    self.windows.splash.loadURL('file://' + __dirname + '/app/views/splash.html');
  },
  createWindow: function(settings){
    var self = this;
    self.settings = settings || self.settings;
    self.winConfig = {
      width: self.settings.width || 800, 
      height: self.settings.height || 600,
      x: self.settings.x || 0,
      y: self.settings.y || 0,
      center: (self.settings.x && self.settings.y) ? false : true,
      frame: false, 
      transparent: true,
      title: 'Invoicer',
      icon: __dirname + '/app/img/icon_64.png',
      show: false
    };
    //
    self.windows.main = new BrowserWindow(self.winConfig);
    self.windows.main.loadURL('file://' + __dirname + '/app/index.html');
    //windows.main.webContents.openDevTools();
    self.windows.main.on('closed', function() {
      //
      self.windows.main = null;
    });
    // on move
    self.windows.main.on('move', function(move) {
      self.bounds = self.windows.main.getBounds();
      if(self.saveTimeout) clearTimeout(self.saveTimeout);
      self.saveSettings(self.bounds);
    });
    // on resize
    self.windows.main.on('resize', function(resize) {
      self.bounds = self.windows.main.getBounds();
      if(self.saveTimeout) clearTimeout(self.saveTimeout);
      self.saveSettings(self.bounds);
    });
    console.log('finish loading');
    setTimeout(function(){
      try{
        self.windows.main.show();
        self.windows.splash.close();
      }catch(err){
        console.log(err);
      }
    },3000);
  },
  loadSettings: function(cb){
    var self = this;
    fs.readJson('./settings.json', function(err, settings) {
      if(err){
        console.log( err );
        cb({
          width: 800, 
          height: 600,
          center: true
        });
      };
      console.log(settings);
      cb(settings);
    });
  },
  saveTimeout: null,
  saveSettings: function(bounds){
    var self = this;
    self.saveTimeout = setTimeout(function(){
      fs.outputJson('./settings.json', bounds, function(err) {
        if( err ){
          console.log(err);
          return
        }
        console.log( bounds );
        console.log('saved window settings');
      });
    }, 500);
  }
};

main.init();