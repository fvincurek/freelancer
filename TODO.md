# ID !URGENT!
vybirad projekty podle _id (nazevprojektu_2cS6t) místo key (array default id: 0,1,2)

# sync
Pokud je nastaven server (adresa) v nastavení, synchronizovat: lokalni data <-> server
mongodb, sailsjs?
pri synchronizaci nahravat s puvodnim _id

# fakturace projektů
pri fakturaci tlacitko "prepocitat", vypocita cas pro projekty u vybranýho klienta ve stanoveným časovým rozmezí,
vypsat tasky (sjednocení podle názvu? - jako subtasky) a hodiny k nim, prjekty a hodiny k nim, a na konec celkove pro celyho klienta.
Klinutim na tlacitko plus u tasku/projektu se položka přidá do faktury a pripocita.

V nastavení bude možnost výběru typu fakturace: 
- hodinová sazba(a přirážka [X%] pro URGENT sazbu) 
- kusově/za práci: 
	uzivatelský hodnoty: { název, cena, dph }

# dph
V nastavení možnost: JSEM/NEJSEM plátce DPH
podle toho se do faktury zapocita dph pro kazdou polozku nebo ne.
- typy dph s ruzhnyma hodnotama
- uzivatelský
	- nazev
	- procento

