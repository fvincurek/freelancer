const remote = require('electron').remote;
const Menu = remote.Menu;
const MenuItem = remote.MenuItem;
const smalltalk = require('smalltalk');
const _ = require('underscore');
const fs = require('fs-extra');
const path = require('path');
require('datejs');

var invoicer = angular.module('invoicer', ["xeditable", 'as.sortable', 'contenteditable', 'ngRoute'])
.filter('numberFixedLen', function () {
  return function (n, len) {
    var num = parseInt(n, 10);
    len = parseInt(len, 10);
    if (isNaN(num) || isNaN(len)) {
      return n;
    }
    num = ''+num;
    while (num.length < len) {
      num = '0'+num;
    }
    return num;
  };
});

invoicer.run(function($rootScope,editableOptions) {
	// DRAG & DROP
	$rootScope.dragControlListeners = {
    accept: function (sourceItemHandleScope, destSortableScope) {
  		//override to determine drag is allowed or not. default is true.
    	return true
  	},
    itemMoved: function (event) {
    	//Do what you want
    },
    orderChanged: function(event) {
    	//Do what you want
    },
    clone: true, //optional param for clone feature.
    allowDuplicates: true //optional param allows duplicates to be dropped.
	};
	//
	editableOptions.theme = 'bs3';
	//
	$rootScope.currentSection = "invoice";
	$rootScope.currentClientId = 0;
	//
	$rootScope.date = new Date();
	$rootScope.dt = new Date();
	$rootScope.menu = false;
	$rootScope.maximized = false;
	$rootScope.showPrice = false;
	$rootScope.showTime = false;
	//
	$rootScope.dateOptions = { formatYear: 'yy', startingDay: 1 };
	$rootScope.format = 'dd-MMMM-yyyy';

	// MAKE UNIQUE ID
	$rootScope.uuid = function(len){
	  len = len || 12;
	  var text = "", possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	  for( var i=0; i < len; i++ ){ text += possible.charAt(Math.floor(Math.random() * possible.length)); };
	  return text;
	};
	console.log( $rootScope.uuid() );

	// SETTINGS
	$rootScope.settings = {
		data: {
			_id: "our_doc",
			our_name: "Your Name",
			our_address: "Address",
			our_bank: "Bank",
			our_accno: "123456789/1234",
			our_platcem: "I am not registered TAX payer.",
			our_phone: "(+420) XXX XXX XXX",
			our_mail: "email@server.cz",
			our_web: "www.youpage.com",
			invoice_no: "2016XX-XXX",
			hour_rate: 150,
			logo_img: "logo.png"
		},
		init: function(){
			var self = this;
			self.db = new PouchDB('settings');
			self.db.get('our_doc', function(err, doc) {
			  if (err) { return console.log(err); }
			  //console.log(doc);
			  self.data = doc;
				self.data.invoice_no = new Date().toString('yyyyMM_d')
			  $rootScope.$apply();
		  	$rootScope.$watch('settings.data', function() {
					//console.log( self.data );
					self.save(self.data);
				});
			  setTimeout(function(){
			  	$rootScope.alert.disabled = false;
			  	$rootScope.$apply();
			  },2000);
			});
		},
		save: function(data){
			var self = this;
			data = data || self.data;
			console.log( data );
			self.db.put(data, function(err, response) {
				if (err) { return console.log(err); }
				//console.log( response );
				self.data._rev = response.rev;
				$rootScope.alert.show({
					title: "Saved",
					message: "Your settings was saved.",
					type: "success",
					timeout: 2
				});
				$rootScope.$apply();
				//console.log( 'setings saved' );
		  });
		},
		saveTimer: null,
		doSave: function(){
			var self = this;
			if( self.saveTimer ) clearTimeout( self.saveTimer );
			self.saveTimer = setTimeout(function(){
				self.save();
			}, 1500);
		}
	}; $rootScope.settings.init();
	// #########
	
	// WINDOW
	$rootScope.window = {
		init: function(){
			var self = this;
			self.window = remote.getCurrentWindow();
			if( !self.menu ){
				console.log('creating menu');
			  self.menu = new Menu();
			  self.menu.append(new MenuItem({ 
					label: 'Print to PDF', 
					click: function() {
						if( !_.isEmpty($rootScope.invoice.currentClient) ){
							smalltalk.prompt('File Name', 'Please input a name for the PDF:', 'faktura_'+$rootScope.settings.data.invoice_no)
							.then(function(value) {
								//console.log(value);
								self.print(value, $rootScope.settings.data.export_path);
								$rootScope.alert.show({
									title: "PDF Exported",
									message: "New PDF was exported to: "+$rootScope.settings.data.export_path+'/'+value+'.pdf',
									type: "success",
									timeout: 6
								});
								$rootScope.$apply();
							}, function() {
								//console.log('cancel');
							});
						}else{
							$rootScope.alert.show({
								title: "Can't export PDF",
								message: "You have to select a client!",
								type: "error",
								timeout: 6
							});
							$rootScope.$apply();
						}
					}
			  }));
			  window.addEventListener('contextmenu', function (e) {
					e.preventDefault();
					self.menu.popup(self.window);
			  }, false);
			}
		},
		print: function(name,export_path){
			var fileName = name ? name : new Date().getTime();
			this.window.webContents.printToPDF({
			  portrait: true
			}, function(err, data) {
			  if( err ) {
			    console.log( err );
			    return
			  }
			  //fs.mkdirsSync('./exports/');
			  var dist = path.resolve(export_path+'/'+fileName+'.pdf');
			  console.log(data);
			  console.log(dist);
			  fs.writeFile(dist, data, function(err) {
			    if(err) console.log('genearte pdf error', err)
			  })
			});
		},
		close: function(){
			//
		  window.close();
		},
		minimize: function(){
			//
		  this.window.minimize(); 
		},
		maximized: false,
		maximize: function(){
		  if(this.maximized){
			  this.window.unmaximize(); 	
			}else{
			  this.window.maximize(); 
			}
			this.maximized = !this.maximized;
		}
	}; $rootScope.window.init();
	// #########

	// CLIENTS
	$rootScope.clients = {
		db: null,
		data: [],
		current: {
			id: 0,
			data: {}
		},
		init: function(){
			//
			var self = this;
			self.get();
			$rootScope.$watch('currentClientId', function() {
				console.log('currentClientId:', $rootScope.currentClientId);
				self.current.data = self.data[$rootScope.currentClientId] ? self.data[$rootScope.currentClientId] : {};
				console.log('self.current.data:', self.current.data);
			});
		},
		get: function(id){
			var self = this;
			self.db = new PouchDB('clients');
			self.db.allDocs({
			  include_docs: true
			}).then(function (result) {
			  //console.log(result);
			  //self.data = result.rows;
			  self.data = _.pluck(result.rows, 'doc');
			  $rootScope.$apply();
			  // handle result
			}).catch(function (err) {
			  console.log(err);
			});
		},
		cacheClients: [],
		find: function(id){
			//
			this.cacheClients[id] = this.cacheClients[id] ? this.cacheClients[id] : _.findWhere(this.data, { _id: id });
			return this.cacheClients[id] ? this.cacheClients[id].company : 'All';
		},
		insert: function(data){
			var self = this;
			//console.log(data);
			if( typeof data !== 'undefined' ){
				data['_id'] = $rootScope.uuid(12);
				self.db.put(data)
				.then(function (response) {
				  console.log('Client '+data.company+' saved!');
				  console.log(response);
				  self.db.get(response.id, function(err, doc) {
						if (err) { return console.log(err); }
						// handle doc
						console.log(doc);
						self.data.push(doc);
						self.new = {};
						self.newVisible = false;
					  $rootScope.alert.show({
							title: "Saved",
							message: "New client saved: "+data.company,
							type: "success",
							timeout: 4
						});
						$rootScope.$apply();
					});
				  // handle response
				}).catch(function (err) {
				  console.log(err);
				  $rootScope.alert.show({
						title: "Didn't catch that.",
						message: "Client can't be created",
						type: "alert",
						timeout: 4
					});
				});
			}else{
			  $rootScope.alert.show({
					title: "Can't do that.",
					message: "I won't create an empty client :/",
					type: "alert",
					timeout: 5
				});
			}
		},
		delete: function(id){
			var self = this;
			console.log(id);
			var doc = _.findWhere(self.data, { _id: id });
			if(typeof doc !== 'undefined' ){
				smalltalk.confirm('Delete?', 'Delete client '+doc.company+'?')
				.then(function() {
					console.log(doc);
					self.db.remove(doc._id, doc._rev, function(err, response) {
						if (err) { return console.log(err); };
						// handle response
						self.get();
						$rootScope.alert.show({
							title: "Deleted",
							message: "Client deleted: "+doc.company,
							type: "success",
							timeout: 4
						});
					});
				}, function() {
					console.log('cancel');
				});
			}
		},
		save: function(id){
			console.log(id);
			var self = this;
			var doc = _.findWhere(self.data, { _id: id });
			doc = angular.copy(doc);
			self.db.put(doc, function(err, response) {
				if (err) { 
					$rootScope.alert.show({
						title: "Try again",
						message: "You must select some client.",
						type: "alert",
						timeout: 4
					});
					return;
				}
				// handle response
				self.get();
				$rootScope.alert.show({
					title: "Updated",
					message: "Client updated: "+doc.company,
					type: "success",
					timeout: 4
				});
				console.log('saved');
		  });
		}
	}; $rootScope.clients.init();
	// #########

	// PROJECTS
	$rootScope.projects = {
		db: null,
		data: [],
		filtered: [],
		current: {
			id: 0,
			data: {}
		},
		init: function(){
			//
			var self = this;
			self.get();
			$rootScope.$watch('currentProjectId', function() {
				console.log('currentProjectId:', $rootScope.currentProjectId);
				self.current.data = self.data[$rootScope.currentProjectId] ? self.data[$rootScope.currentProjectId] : {};
				console.log('self.current.data:', self.current.data);
			});
		},
		get: function(id,force_reload){
			var self = this;
			self.db = new PouchDB('projects');
			self.db.allDocs({
			  include_docs: true
			}).then(function (result) {
			  console.log(result);
			  self.data = _.pluck(result.rows, 'doc');//result.rows;
			  self.filtered = _.isEmpty(self.filtered) || force_reload ? self.data : self.filtered;
			  $rootScope.$apply();
			  // handle result
			}).catch(function (err) {
			  console.log(err);
			});
		},
		cacheProjects: [],
		find: function(id){
			//
			this.cacheProjects[id] = this.cacheProjects[id] ? this.cacheProjects[id] : _.findWhere(this.data, { _id: id });
			//console.log( project );
			return this.cacheProjects[id] ? this.cacheProjects[id].name : false;
		},
		insert: function(data){
			var self = this;
			console.log(data);
			data['_id'] = $rootScope.uuid(12);
			self.db.put(data)
			.then(function (response) {
			  console.log('Project '+data.name+' saved!');
			  console.log(response);
			  self.db.get(response.id, function(err, doc) {
					if (err) { return console.log(err); }
					// handle doc
					console.log(doc);
					self.data.push(doc);
					self.new = {};
					self.newVisible = false;
					self.get(false,true);
					$rootScope.alert.show({
						title: "Saved",
						message: "New project saved: "+data.name,
						type: "success",
						timeout: 4
					});
					$rootScope.stopwatch.task.project = data['_id'];
					$rootScope.projects.filtered = _.where( $rootScope.projects.data, { client: data['client'] });
					$rootScope.$apply();
				});
			  // handle response
			}).catch(function (err) {
			  console.log(err);
			});
		},
		delete: function(id){
			var self = this;
			var doc = _.findWhere(self.data, { _id: id });
			console.log(doc);
			if(typeof doc !== 'undefined' ){
				smalltalk.confirm('Delete?', 'Delete project '+doc.name+'?')
				.then(function() {
					console.log(doc);
					self.db.remove(doc._id, doc._rev, function(err, response) {
						if (err) { return console.log(err); };
						// handle response
						self.get(false,true);
						$rootScope.alert.show({
							title: "Deleted",
							message: "Project deleted: "+doc.name,
							type: "success",
							timeout: 4
						});
					});
				}, function() {
					console.log('cancel');
				});
			}
		},
		save: function(doc){
			console.log(doc);
			doc = angular.copy(doc);
			var self = this;
			self.db.put(doc, function(err, response) {
				if (err) { return console.log(err); }
				// handle response
				self.get();
				console.log('saved');
				$rootScope.alert.show({
					title: "Updated",
					message: "Project updated: "+doc.name,
					type: "success",
					timeout: 4
				});
		  });
		}
	}; $rootScope.projects.init();
	// #########

	// TASKS
	$rootScope.tasks = {
		data: [],
		filtered: [],
		init: function(){
			var self = this;
			self.db = new PouchDB('tasks');
			self.get();
		},
		get: function(){
			var self = this;
			self.db.allDocs({
			  include_docs: true
			}).then(function (result) {
			  console.log(result);
			  self.data = _.pluck(result.rows, 'doc');
			  _.each(self.data, function(task, key){
			  	if(!task.newTime){
			  		task.newTime = $rootScope.stopwatch.formatTime(task.tracked);
			  		self.save(task);
			  	}
			  });
			  self.filter();
			  $rootScope.$apply();
			  // handle result
			}).catch(function (err) {
			  console.log(err);
			});
		},
		filter: function(client,project){
			client = client || $rootScope.stopwatch.task.client;
			project = project || $rootScope.stopwatch.task.project;
			if(project === 'all'){
				if(client === 'all'){
					$rootScope.tasks.filtered = $rootScope.tasks.data;
				}else{
					$rootScope.tasks.filtered = _.where($rootScope.tasks.data, { client: client });
				}
			}else{
				if(client !== 'all'){
					$rootScope.tasks.filtered = _.where($rootScope.tasks.data, { client: client, project: project });
				}else{
					$rootScope.tasks.filtered = _.where($rootScope.tasks.data, { project: project });
				}
			}
		},
		add: function(data){
			var self = this;
			console.log(data);
			data['_id'] = $rootScope.uuid(12);
			console.log( data );
			self.db.put(data)
			.then(function (response) {
			  console.log('Task '+data.name+' saved!');
			  console.log(response);
			  self.db.get(response.id, function(err, doc) {
					if (err) { return console.log(err); }
					// handle doc
					console.log(doc);
					self.data.push(doc);
					self.new = {};
					self.newVisible = false;
					self.filter();
					$rootScope.$apply();
				});
			  // handle response
			}).catch(function (err) {
			  console.log(err);
			});
		},
		saveDelay: null,
		save: function(doc){
			var adoc = angular.copy(doc);
			var self = this;
			if(this.saveDelay) clearTimeout(this.saveDelay);
			this.saveDelay = setTimeout(function(){
				self.db.put(adoc, function(err, response) {
					if (err) { return console.log(err); }
					// handle response
					doc._rev = response.rev;
					self.filter();
					console.log(response);
					$rootScope.alert.show({
						title: "Updated",
						message: "Task updated: "+doc.name,
						type: "success",
						timeout: 2
					});
					$rootScope.$apply();
			  });
			}, 1500);
		},
		delete: function(doc){
			var self = this;
			smalltalk.confirm('Delete?', 'Delete task '+doc.name+'?')
			.then(function() {
				console.log(doc);
				self.db.remove(doc, function(err, response) {
					if (err) { return console.log(err); };
					// handle response
					self.get();
				});
			}, function() {
				console.log('cancel');
			});
		}
	}; $rootScope.tasks.init();
	// #########

	// STOPWATCH
	$rootScope.minitracker = {
		show: false,
		symbol: function(){
			return this.show ? "▲" : "▼";
		}
	};
	$rootScope.stopwatch = {
		startText: 'play',
		init: function() {
			this.tracking = this.tracking || false;
			this.pausedTime = this.pausedTime || false;
			this.trackedTime = this.trackedTime || 0;
			this.input = this.input || {};
			this.buttons = this.buttons || {
				stop: {
					disabled: true
				},
				pause: {
					disabled: true
				},
				start: {
					disabled: false
				}
			};
			this.task = this.task || {
				client: 'all',
				project: 'all'
			};
			var self = this;
			self.data = [];
			$rootScope.$watch('stopwatch.task.client', function() {
				console.log('stopwatch.task.client:', self.task.client);
				if(self.task.client === 'all'){
					$rootScope.tasks.filtered = $rootScope.tasks.data;
					$rootScope.projects.filtered = $rootScope.projects.data;
				}else{
					$rootScope.tasks.filtered = _.where($rootScope.tasks.data, { client: self.task.client });
					$rootScope.projects.filtered = _.where( $rootScope.projects.data, { client: self.task.client });
				}
				if(self.tracking) self.saveTemp();
			});
			$rootScope.$watch('stopwatch.task.project', function() {
				console.log('stopwatch.task.project:', self.task.client);
				$rootScope.tasks.filter(self.task.client, self.task.project);
				if(self.tracking) self.saveTemp();
				$rootScope.showProjects = false;
			});
			var saveTimeout = false;
			$rootScope.$watch('stopwatch.task.name', function() {
				console.log('stopwatch.task.name:', self.task.name);
				if(saveTimeout){
					clearTimeout(saveTimeout);
				}
				saveTimeout = setTimeout(function(){
					if(self.tracking) self.saveTemp();
					console.log('Temp was updated.');
				},1500);
			});
			var saveTimeout = false;
			$rootScope.$watch('stopwatch.task.urgent', function() {
				console.log('stopwatch.task.urgent:', self.task.urgent);
				if(saveTimeout){
					clearTimeout(saveTimeout);
				}
				saveTimeout = setTimeout(function(){
					if(self.tracking) self.saveTemp();
					console.log('Temp was updated.');
				},1500);
			});
			self.trackedTime = 0;
			fs.readJson('./cache/temp_task.json', function(err, packageObj) {
			  console.log('Temp Task: ', packageObj); 
			  self.temp_task = packageObj;
			  if( packageObj && !packageObj.empty ){
			  	self.start(true,packageObj,true);
			  }
			  $rootScope.$apply();
			});
		},
		start: function(cont, cont_task, autoreturn) {
			this.currentTime = new Date(); //zjistit aktuální čas
			if( this.tracking && !this.paused ){
				this.stop();
			}else{
				this.paused = false;
			};
			if (!this.pausedTime){
				// fill current task
				this.task.start = this.currentTime;
				this.pausedTimeDur = 0;
				if (cont){
					console.log('Continue: ', cont_task);
					this.task.client = cont_task.client;
					this.task.project = cont_task.project;
					this.task.name = cont_task.name;
					this.task.urgent = cont_task.urgent;
					this.task.start = autoreturn ? new Date(cont_task.start) : this.task.start;
					this.clientFilter = cont_task.clientName ? cont_task.clientName : '';
					this.projectFilter = cont_task.projectName ? cont_task.projectName : '';
					if( autoreturn ){
					  $rootScope.alert.show({
							title: "Continuing",
							message: "Continuing in previous task: "+cont_task.name,
							type: "success",
							timeout: 5
						});
					}
				}
				this.startTime = this.task.start; //definovat čas startu
			} else {
				this.pausedTimeDur = this.pausedTimeDur + (this.currentTime - this.pausedTime);
				this.pausedTime = false;
				this.exportTimeFormat = this.pausedTimeDur;
				console.log('Paused time: '+this.pausedTimeDur);
				console.log('Paused at: '+this.exportTimeFormat);
			}
			//
			this.ticker();
			this.tracking = true;
			//
			this.buttons.stop.disabled = false;
			this.buttons.pause.disabled = false;
			this.buttons.start.disabled = true;
			this.startText = 'pause';
			// Save temp task to start if closed unexpectedly
			this.saveTemp();
		},
		stop: function() {
			if(this.tracking){
				this.currentTime = new Date();
				if (this.pausedTime){
					this.pausedTimeDur = +this.pausedTimeDur + (this.currentTime - this.pausedTime);
					console.log(this.pausedTimeDur);
					this.pausedTime = false;
				};
				//
				console.log(this.task.urgent);
				this.tracked = (this.currentTime - this.startTime) - this.pausedTimeDur;
				// add shit to current task
				this.task.tracked = this.tracked;
				this.task.newTime = this.formatTime(this.tracked);
				this.task.pausetime = this.pausedTimeDur;
				this.task.end = this.currentTime;
				this.addEntry();
				// vyčist timer
				this.trackedTime = 0;
				this.pausedTime = 0;
				this.tracked = 0;
				//
				this.input.task_input = '';
				this.input.projectv = '';
				this.input.task_input = '';
				this.input.project = '';
				clearInterval(this.timer);
				//
				this.buttons.stop.disabled = true;
				this.buttons.pause.disabled = true;
				this.buttons.start.disabled = false;
				this.tracking = false;
				this.startText = 'play';
				//
				this.clientFilter = '';
				this.projectFilter = '';
				// Delete temp task
				this.deleteTemp();
			}
		},
		addEntry: function(){
			console.log( this.task );
			$rootScope.tasks.add(this.task);
		},
		playPause: function() {
			if (!this.paused && this.tracking){
				this.pausedTime = new Date(); //zjistit čas
				this.paused = true;
				clearInterval(this.timer);
				this.buttons.start.disabled = true;
				this.startText = 'play';
			} else {
				this.startText = 'pause';
				this.start();
			}
		},
		ticker: function() {
			var self = this;
			self.trackedTime = (self.currentTime - self.startTime) - self.pausedTimeDur;
			this.timer = setInterval(function(){
				self.currentTime = new Date(); //zjistit čas
				self.trackedTime = (self.currentTime - self.startTime) - self.pausedTimeDur;
				$rootScope.$apply();
			}, 250);
		},
		formatDate: function(date){
			//
			return new Date(date).toString('dd-MMMM-yyyy HH:mm');
		},
		formatTime: function(time){
			var t = this.convertMS(time);
			if (typeof t.ms !== 'undefined'){
					t.s = t.s < 10 ? '0'+t.s : t.s;
				var seconds = t.s !== 0 ? t.s : '00';
					t.m = t.m < 10 ? '0'+t.m : t.m;
				var minutes = t.m !== '00' ? t.m+':' : '00:';
					//t.h = t.h < 10 ? '0'+t.h : t.h;
				var hours = t.h > 0 ? t.h+':' : '';
				var koncovka = '';
				//koncovka = hours !== '' ? '' : ' min';
				return hours+minutes+seconds+koncovka;
			}else {
				return '';
			};
		},
		convertMS: function(ms) {
			ms = ms ? ms : 0;
			var d, h, m, s, formated;
			s = Math.floor(ms / 1000);
			m = Math.floor(s / 60);
			h = Math.floor(m / 60);
			d = Math.floor(h / 24);
			s = s % 60; //if(s<10 && s!==0){s=s;};
			m = m % 60; //if(m<10 && m!==0){m="0"+m;};
			h = h;// % 24; //if(h<10 && h!==0){h="0"+h;};
			d = d % 24; //if(d<10 && d!==0){d="0"+d;};
			return { d: d, h: h, m: m, s: s, ms: ms};
		},
		parseTimeout: null,
		updateTask: function(task){
			var self = this;
			if(this.parseTimeout) clearTimeout(this.parseTimeout);
			this.parseTimeout = setTimeout(function(){
				console.log( task.tracked );
				task.tracked = self.parseTime(task.newTime)
				console.log( task.tracked );
				console.log( self.convertMS( self.parseTime(task.newTime) ) );
				$rootScope.tasks.save(task);
			},500);
		},
		parseTime: function(timeString){
		  timeString = timeString.split(':');
		  console.log(timeString.length);
		  if(timeString.length === 3){
		    console.log(timeString[0],timeString[1],timeString[2]);
		    var complete = (timeString[0]*60*60*1000) + (timeString[1]*60*1000) + (timeString[2]*1000);
		  }else if(timeString.length === 2){
		    var complete = (timeString[0]*60*1000) + (timeString[1]*1000);	
		  }else{
		    var complete = (timeString[0]*1000);
		  }
		  return complete;
		},
		saveTemp: function(){
			var self = this;
			this.task.clientName = $rootScope.clients.find(this.task.client);
			this.task.projectName = $rootScope.projects.find(this.task.project);
			fs.outputJson('./cache/temp_task.json', this.task, function(err) {
			  if( err ){
			  	console.log(err);
			  	return
			  }
				console.log( 'Temp saved.' );
			});
		},
		deleteTemp: function(){
			var self = this;
			fs.outputJson('./cache/temp_task.json', {empty: true}, function(err) {
			  if( err ){
			  	console.log(err);
			  	return
			  }
			  self.task = {};
			  console.log('Temp erased.');
			});
		},
		fitText: function(text,id){
			console.log( text, id );
			document.getElementById(id).style['min-width'] = text.length + 'rem';
		}
	}; $rootScope.stopwatch.init();
	// #########

	// INVOICE
	$rootScope.invoice = {
		data: {
			line_items: []
		},
		haveSomePrice: function(){
			var somePrice = false;
			_.each( this.data.line_items, function(value,key){
				//console.log( 'key:',key, 'value:',value );
				if( !somePrice && value.showPrice ){
					somePrice = true;
				}
			});
			return somePrice
		},
		init: function(){
			// Watch for date change and update due_date automatically
			var due_length = 1000*60*60*24*14; // 14 dní
			$rootScope.inv_due = new Date($rootScope.date.getTime() + due_length); 
			$rootScope.$watch('date', function() {
				$rootScope.getDue();
			});
			$rootScope.getDue = function(){
				$rootScope.inv_due = new Date($rootScope.date.getTime() + due_length);
			};
			this.fromDate = new Date.today().add(-30).day();
			this.toDate = new Date.today();
		},
		getTotal: function(){
			var total=0;
			for(var i=0; i < $rootScope.invoice.data.line_items.length; i++) {
				var line = $rootScope.invoice.data.line_items[i];
				if(line.showPrice){
					total += (line.price * 1);
				}
			}
			return total;
		},
		getSummary: function(){
			var self = this;
			console.log( self.fromDate );
			console.log( self.toDate );
			// if we have client
			if( typeof this.currentClient !== 'undefined' ){
				var selection = _.where($rootScope.tasks.data, { client: this.currentClient._id } );
				self.sorted = {};
				var projects = _.uniq(_.pluck( selection, 'project'));
				// calculate time for each project under current client
				_.each( projects, function(key,value){
					var project = key === 'all' ? {_id:'all',name:'All'} : _.findWhere( $rootScope.projects.data, { _id: key});
					if( project ){
						var project_tasks = _.where( selection, { project: project._id });
						//var times = _.pluck( project_tasks, 'tracked' );
						var project_tasks_time = 0;//_.reduce(times, function(memo, num){ return parseInt(memo) + parseInt(num); }, 0);
						// tasks is project
						var tasks_names = _.uniq(_.pluck( project_tasks, 'name'));
						var grouped_tasks = {};
						// calculate time for each task by name under current project
						_.each(tasks_names, function(key,value){
							var tasks = _.where( project_tasks, { name: key });
							var tasks_times = [];
							_.each( tasks, function( task, id ){
								var startDay = new Date(task.start);
								if( startDay.between(self.fromDate, self.toDate) ){
									tasks_times.push(task.tracked);
								};
							});
							//var tasks_times = _.pluck( tasks, 'tracked' );
							var group_tasks_time = _.reduce(tasks_times, function(memo, num){ return parseInt(memo) + parseInt(num); }, 0);
							// set tasks grouped by name in one
							if( group_tasks_time > 0 ){
								grouped_tasks[key] = {
									name: key,
									tracked: group_tasks_time,
									trackedReadable: $rootScope.stopwatch.formatTime(group_tasks_time),
									price: self.calculateMoney(group_tasks_time),
									showPrice: true
								}
							}
							project_tasks_time += parseInt(group_tasks_time); 
						});
						if( project_tasks_time > 0 ){
							// set sorted array to hold projects with tasks
							self.sorted[project.name] = {
								tracked: project_tasks_time,
								trackedReadable: $rootScope.stopwatch.formatTime(project_tasks_time),
								name: project.name,
								price: self.calculateMoney(project_tasks_time),
								tasks: grouped_tasks,
								showPrice: true
							};
						}
					}
				});
				$rootScope.showEditor = true;
				//$rootScope.$apply();
				this.summary = self.sorted;
			}else{
				// if we dont have client
				$rootScope.alert.show({
					title: "Whoops",
					message: "You forgot to select client.",
					type: "alert",
					timeout: 4
				});
			}
		},
		calculateMoney: function(time){
			//console.log( time + '/' + $rootScope.settings.data.hour_rate );
			var money = ((parseInt(time)/1000) * (parseInt($rootScope.settings.data.hour_rate)/60/60)).toFixed(2);
			return money;
		},
		addFromSum: function(item){
			var self = this;
			var newItem = angular.copy(item);
			newItem.price = self.calculateMoney(newItem.tracked);
			$rootScope.invoice.data.line_items.push(newItem);
		},
		setClient: function(id){
			this.currentClient = _.findWhere($rootScope.clients.data, { _id: id } );
			console.log(this.currentClient);
		},
		addItem: function() {
			if( !_.isEmpty($rootScope.invoice.data.temp) ){
				$rootScope.invoice.data.line_items.push($rootScope.invoice.data.temp);
				$rootScope.invoice.data.temp = {};
			}else{
				$rootScope.alert.show({
					title: "Alert",
					message: "Can't add empty item.",
					type: "alert",
					timeout: 2
				});
			}
		},
		removeItem: function(idx) {
			//
			$rootScope.invoice.data.line_items.splice(idx, 1);
		},
		checkValue: function(data) {
			if (isNaN(data)) {
				return "Numbers only please.";
			}
		}
	}; $rootScope.invoice.init();
	// #########
  
  // ALERT
	$rootScope.alert = {
  	data: [],
  	disabled: true,
  	show: function(config){
  		if(!config){
  			console.log('ng-alert: no data given');
  			return
  		};
  		var self = this;
  		if( !self.disabled ){
	  		var timeout = config.timeout || false;
	  		var id = $rootScope.uuid();
	  		var item = {
	  			id: id,
	  			title: config.title,
	  			message: config.message,
	  			type: config.type
	  		};
	  		this.data.push(item);
	  		console.log( id );
	  		console.log( timeout );
	  		if( timeout ){
	  			self.update(id,timeout);
		  		setTimeout(function(){
		  			self.hide(id);
		  		},timeout*1000);
	  		}
  		}
  	},
  	hide: function(id){
  		var self = this;
  		//$rootScope.$apply();
  		var arrId = _.indexOf(self.data, _.findWhere(self.data, { id: id }));
			try{
				console.log( self.data, self.data[arrId] );
	  		self.data[arrId].animation = "toleft";
	  		setTimeout(function(){
	  			self.data.splice(arrId, 1);
	  			clearTimeout(self.watchers[id]);
					$rootScope.$apply();
  			},500);
			}catch(err){
				console.log( err );
				$rootScope.$apply();
			}
  	},
  	watchers: [],
  	update: function(id,timeout){
  		var self = this;
  		var step = 0;
  		self.watchers[id] = setInterval(function(){
  			try{
					var arrId = _.indexOf(self.data, _.findWhere(self.data, { id: id }));
					//console.log( arrId );
	  			step += 0.1;
	  			var progress = (100/timeout) * step;
	  			if( progress < 100 ){
	  				self.data[arrId].progress = progress;
	  			}else{
		  			clearTimeout(self.watchers[id]);		
	  			}
  			}catch(err){
  				console.log( err );
  				clearTimeout(self.watchers[id]);
  				self.hide(id);
  			}
  			$rootScope.$apply();
  		},100);
  	}
  };
	// #########

	// UPLOADS
	$rootScope.sUpload = {
		config: {},
		iG: 0,
		container: [],
		callback: function(){},
		init: function( id, config, callback ){
			var self = this;
		  // set and merge config
		  if( typeof config === 'function' ){
				callback = config;
				config = {};
		  };
		  console.log( config );
		  self.config = config ? self.merge( self.config, config ) : self.config;
		  console.log( self.config );
		  // set callback if passed to function
		  self.callback = callback || self.callback;
		  // get the element for drop and upload form
		  self.container = id ? document.getElementById( id ) : document;
		  // create hidden input field
		  self.appendInputField();
		  // check if browser supports File API
		  if (window.File && window.FileReader && window.FileList && window.Blob) {
				self.container.addEventListener('dragover', self.dragOver, false);
				self.container.addEventListener('dragleave', self.dragEnd, false);
				self.container.addEventListener('drop', self.fileSelect, false);
		  } else {
				alert('The File APIs are not fully supported in this browser.');
		  }
		},
		toDataURL: function(url, callback, outputFormat){
		  // function to encode file data to base64 encoded string
		  // read binary data
		  var bitmap = fs.readFileSync(url);
		  // convert binary data to base64 encoded string
		  var base64url = new Buffer(bitmap).toString('base64');
		  return base64url;
		},
		appendInputField: function(){
			var self = this;
		  var form = '<form id="sUpload-form" method="post" style="visibility:hidden;position:absolute;">'+
								  '<input type="submit" value="Upload">'+
								  '<input id="sUpload-filef" type="file" name="file">'+
								 '</form>';
		  self.container.insertAdjacentHTML( 'afterbegin', form );
		  self.container.insertAdjacentHTML( 'afterbegin', '<div id="click-to-upload" style="position:absolute;top:0;left:0;right:0;bottom:0;width:100%;height:100%;display:block;"></div>' );
		  self.clickToUpload = document.getElementById('click-to-upload');
		  self.inputField = document.getElementById('sUpload-filef');

		  if(self.config.action !== 'folder'){
			  self.clickToUpload.addEventListener('click', function(){
					self.inputField.click();
			  });
		  }
		  self.inputField.onchange = function( data ) {
				console.log( data );
				self.fileSelect( data, true );
		  };
		},
		error: function( err ){
		  //
		  console.log( err );
		},
		abort: function( err ){
		  //
		  console.log( err );
		},
		dragOver: function( ev ){
		  ev.stopPropagation();
		  ev.preventDefault();
		  ev.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
		},
		dragEnd: function( ev ){
		  ev.stopPropagation();
		  ev.preventDefault();
		},
		fileSelect: function( ev, form ){
			var self = $rootScope.sUpload;
		  ev.stopPropagation();
		  ev.preventDefault();
		  //console.log( ev );
		  //console.log( self.config );
		  var action = self.config ? self.config.action : 'base64';
		  var files = form ? ev.target.files : ev.dataTransfer.files; // FileList object.
		  if( action === 'base64' ){
			  var f = files[0];
			  var bitmap = fs.readFileSync(f.path);
			  // convert binary data to base64 encoded string
			  var base64url = 'data:image/gif;base64,' + new Buffer(bitmap).toString('base64');
			  //
				self.callback( base64url );
		  }else if( action === 'folder'){
		  	self.callback(files);
		  };
		},
		returnFileExtension: function(fileName) {
		  //
		  return fileName.substr((~-fileName.lastIndexOf(".") >>> 0) + 2).toLowerCase();
		},
		bytesToSize: function(bytes) {
		  if(bytes === 0) return '0 Byte';
		  var k = 1000;
		  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
		  var i = Math.floor(Math.log(bytes) / Math.log(k));
		  return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
		},
		merge: function(obj1,obj2){
		  // merges two arrays and replaces values in obj1 with obj2
		  var obj3 = {};
		  for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
		  for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
		  return obj3;
		}
  };
	// #########

	// Show the content after all the Model View stuff syncs.
	$rootScope.contentLoaded = true;
});

invoicer.config(['$routeProvider',
function($routeProvider) {
  $routeProvider.
    when('/stopwatch', {
      templateUrl: 'views/stopwatch.html',
      controller: 'StopwatchCtrl'
    }).
    when('/invoice', {
      templateUrl: 'views/invoice.html',
      controller: 'InvoiceCtrl'
    }).
    when('/settings', {
      templateUrl: 'views/settings.html',
      controller: 'SettingsCtrl'
    }).
    otherwise({
      redirectTo: '/stopwatch'
    });
}]);