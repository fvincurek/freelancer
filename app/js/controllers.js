
invoicer.controller('MainCtrl', function($rootScope){

});

invoicer.controller('InvoiceCtrl', function($rootScope){
	$rootScope.currentSection = 'invoice';
});

invoicer.controller('StopwatchCtrl', function($rootScope){
	$rootScope.currentSection = 'stopwatch';

});

invoicer.controller('SettingsCtrl', function($rootScope){
	$rootScope.currentSection = 'settings';
	// SETTINGS
	$rootScope.sUpload.init('export-folder', { action: 'folder' }, function(folder){
		//console.log(folder);
  	$rootScope.settings.data.export_path = folder[0].path;
		$rootScope.settings.save();
  });
	// #########
});

invoicer.controller('UploadCtrl', function($rootScope){ 
  $rootScope.sUpload.init('logo-upload', { action: 'base64' }, function(name){
		//console.log(name+' Uploaded');
		$rootScope.settings.data.logo_image = name;
		$rootScope.settings.save();
  });
});