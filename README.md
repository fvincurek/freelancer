## Invoicer
### Description
[Original inspiration](http://randysofia.com/2014/06/20/tool-for-invoicing-customers/)

This application is for offline time tracking and invoice creating application. It lets you track time of your projects, create Clients and add projects and task to them, automatically calculates time spend on projects. You can save Clients details and reuse them for future invoices. It's ment for freelancers or small teams. No online support yet, but if I get the time, I'll be writing nodejs server for syncing with mobile app (in the future).

I'm giving this here as it is, do whatever you want with it but be sure you disclose the source ;)

### Running
```
git clone git@bitbucket.org:fvincurek/invoicer.git
cd invoicer
npm install && npm start
```

##š Building
###Installing FPM
```
sudo apt-get update
sudo apt-get install ruby-dev build-essential
sudo gem install fpm
```

For Building your own installer, please continue to [electron-builder](https://www.npmjs.com/package/electron-builder).

### Installers
You can find Linux and Windows installers in the builds directory.

### Licence
Published under [GNU GPLv3](http://choosealicense.com/licenses/gpl-3.0/#)

### Disclaimer
This app is in development, usable, with basic features, but still in development.